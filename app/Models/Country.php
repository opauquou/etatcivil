<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $fillable = ['code', 'libelle', 'prefix', 'created_at', 'updated_at'];
    
    const DEFAULT_COUNTRY = 45;
    
    public function cities(){
        return $this->hasMany(City::class, 'country_id', 'id');
    }
}
