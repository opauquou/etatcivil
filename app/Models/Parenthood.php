<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Parenthood extends Model
{
    protected $table = 'parenthoods';
    protected $fillable = ['id', 'name', 'libelle', 'created_at', 'updated_at'];
}
