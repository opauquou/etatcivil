<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MissingCertificate extends Model
{
    const TYPE_CERT = 'missing';
    const FILLABLE = [
        'id', 'type_cert', 'mc_missing_date', 'mc_missing_country_id', 'mc_missing_city_id',
        'mc_missing_lname', 'mc_missing_fname', 'mc_missing_birth_date',
        'mc_missing_birth_country_id', 'mc_missing_birth_city_id',
        'asker_lname', 'asker_fname', 'asker_phone', 'asker_email', 'parenthood_id',
        'created_at', 'updated_at'
    ];
    
    protected $table = 'certificates';
    
    protected $fillable = self::FILLABLE;
    
    protected $hidden = [
        'bc_lname', 'bc_fname', 'bc_birth_date', 'bc_birth_country_id', 'bc_birth_city_id',
        'bc_father_lname', 'bc_father_fname', 'bc_mother_lname', 'bc_mother_fname',
    
        'wc_weeding_date', 'wc_weeding_country_id', 'wc_weeding_city_id',
        'wc_husband_lname', 'wc_husband_fname', 'wc_husband_birth_date',
        'wc_wife_lname', 'wc_wife_fname', 'wc_wife_birth_date',
    ];
    
    public static function query()
    {
        return (new static)->newQuery()->where('type_cert', self::TYPE_CERT);
    }
    
    public static function all($columns = null){
        $columns = is_array($columns) ? $columns : self::FILLABLE;
        return parent::all($columns);
    }
}
