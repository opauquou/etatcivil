<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\MissingCertificate;
use App\Models\Parenthood;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class MissingCertificateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['create', 'store']);
    }
    
    public function index()
    {
        return view('pages.backend.certificates.missing');
    }
    
    public function getData()
    {
        $certificates = MissingCertificate::all();
    
        return DataTables::of($certificates)
            ->editColumn('created_at', function ($certificate){
                return Carbon::parse($certificate->created_at)->format('Y-m-d');
            })
            ->addColumn('actions', function ($certificate){
                return '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" title="Détails">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Communication\Clipboard-list.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"/>
                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"/>
                                        <rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"/>
                                        <rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"/>
                                        <rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"/>
                                        <rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"/>
                                        <rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"/>
                                        <rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"/>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </a>
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">
                            <span class="svg-icon svg-icon-md">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
                                        <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
                                    </g>
                                </svg>
                            </span>
                        </a>';
            })
            ->escapeColumns([])
            ->make(true);
    }
    
    public function create(){
        $countries = Country::all();
        $cities = Country::whereId(Country::DEFAULT_COUNTRY)->first()->cities;
        $parentHoods = Parenthood::orderBy('id', 'ASC')->get();
        return view('pages.frontend.certificates.missing', compact('countries', 'cities', 'parentHoods'));
    }
    
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            
            $data = [
                'type_cert' => $request['typeCert'],
                'mc_missing_date' => Carbon::parse($request['missing_date'])->format('Y-m-d'),
                'mc_missing_country_id' => Country::whereCode($request['missing_country'])->first()->id,
                'mc_missing_city_id' => $request['missing_city'],
                'mc_missing_lname' => $request['missing_lname'],
                'mc_missing_fname' => $request['missing_fname'],
                'mc_missing_birth_date' => Carbon::parse($request['missing_birth_date'])->format('Y-m-d'),
                'mc_missing_birth_country_id' => Country::whereCode($request['missing_birth_country'])->first()->id,
                'mc_missing_birth_city_id' => $request['missing_birth_city'],
                'asker_lname' => $request['asker_lname'],
                'asker_fname' => $request['asker_fname'],
                'asker_phone' => $request['asker_phone'],
                'asker_email' => $request['asker_email'],
                'parenthood_id' => Parenthood::whereName($request['parenthood'])->first()->id
            ];
            
            MissingCertificate::create($data);
            DB::commit();
            
            session()->flash('success', 'Votre demande de certificat a bien été envoyée.');
            return redirect()->route('home');
        }
        catch (\Exception $e){
            DB::rollBack();
            session()->flash('error', 'Une erreur est survénue. Veuillez réésayer plus tard.');
            return back()->withInput($request->all());
        }
    }
}
