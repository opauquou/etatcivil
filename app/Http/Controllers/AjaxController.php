<?php


namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\Parenthood;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function citiesOfCountry(Request $request){
        $cities = Country::whereCode($request['country_code'])->first()->cities;
        return response()->json($cities);
    }
    
    public function getCountry(Request $request){
        $country = Country::whereCode($request['country_code'])->first();
        return response()->json($country);
    }
    
    public function getCity(Request $request){
        $city = City::whereId($request['city_id'])->first();
        return response()->json($city);
    }
    
    public function getParenthood(Request $request){
        $parenthood = Parenthood::whereName($request['name'])->first();
        return response()->json($parenthood);
    }
}
