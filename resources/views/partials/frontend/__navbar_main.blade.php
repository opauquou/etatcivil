@php
    $routeName = Illuminate\Support\Facades\Route::currentRouteName();
@endphp
<div id="kt_header" class="header flex-column header-fixed">
    <!--begin::Top-->
    <div class="header-top">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Left-->
            <div class="d-none d-lg-flex align-items-center mr-3">
                <!--begin::Logo-->
                <a href="{{ route('home') }}" class="mr-20">
                    <img alt="Logo" src="{{ asset('assets/media/logos/logo-letter-9.png') }}" class="max-h-35px" />
                </a>
                <!--end::Logo-->
                <!--begin::Tab Navs(for desktop mode)-->
                <ul class="header-tabs nav align-self-end font-size-lg" role="tablist">
                    <!--begin::Item-->
                    <li class="nav-item">
                        <a href="{{ route('home') }}" class="nav-link py-4 px-6{{ $routeName == 'home' ? ' active' : ''}}">Accueil</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="nav-item mr-3">
                        @php
                            $isCertPage = substr($routeName, 0, strlen('certificate')) == 'certificate';
                        @endphp
                        <a href="javascript:;" class="nav-link py-4 px-6{{ $isCertPage ? ' active' : '' }}" data-toggle="tab" data-target="#kt_header_tab_certificate" role="tab">Actes Civils</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="nav-item mr-3">
                        <a href="{{ route('contact') }}" class="nav-link py-4 px-6{{ $routeName == 'contact' ? ' active' : ''}}">Contact</a>
                    </li>
                    <!--end::Item-->
                </ul>
                <!--begin::Tab Navs-->
            </div>
            <!--end::Left-->
            <!--begin::Topbar-->
            <div class="topbar bg-primary">

                @include('partials.frontend.__feature_search')

                {{--@include('partials.backend.__feature_notification')--}}

                {{--@include('partials.backend.__feature_quick_action')--}}

                {{--@include('partials.backend.__feature_cart')--}}

                {{--@include('partials.backend.__icon_quick_panel')--}}

                {{--@include('partials.backend.__icon_chat_panel')--}}

                {{--@include('partials.frontend.__icon_user_panel')--}}

</div>
<!--end::Topbar-->
</div>
<!--end::Container-->
</div>
<!--end::Top-->

@include('partials.frontend.__navbar_main_bottom')
</div>
