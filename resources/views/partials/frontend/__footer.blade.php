<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold">Copyright ©</span>
            <a href="{{ route('home') }}" target="_blank" class="text-dark-75 text-hover-primary">{{ config('app.name') }}</a>
            <span class="text-muted font-weight-bold mr-2"> {{ \Date('Y') }}</span>
        </div>
        <!--end::Copyright-->
        <!--begin::Nav-->
        <div class="nav nav-dark order-1 order-md-2">
            <a href="{{ route('home') }}" class="nav-link pr-3 pl-0">Accueil</a>
            <a href="{{ route('contact') }}" class="nav-link pl-3 pr-0">Contact</a>
        </div>
        <!--end::Nav-->
    </div>
    <!--end::Container-->
</div>
