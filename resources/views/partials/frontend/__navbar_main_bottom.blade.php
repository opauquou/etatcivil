<!--begin::Bottom-->
<div class="header-bottom">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Header Menu Wrapper-->
            <div class="header-navs header-navs-left" id="kt_header_navs">
                <!--begin::Tab Navs(for tablet and mobile modes)-->
                <ul class="header-tabs p-5 p-lg-0 d-flex d-lg-none nav nav-bold nav-tabs" role="tablist">
                    <!--begin::Item-->
                    <li class="nav-item mr-2">
                        <a href="{{ route('home') }}" class="nav-link btn btn-clean{{ $routeName == 'home' ? ' active' : ''}}">Accueil</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="nav-item mr-2">
                        <a href="javascript:;" class="nav-link btn btn-clean{{ $isCertPage ? ' active' : '' }}" data-toggle="tab" data-target="#kt_header_tab_certificate" role="tab">Actes Civils</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="nav-item mr-2">
                        <a href="{{ route('contact') }}" class="nav-link btn btn-clean{{ $routeName == 'contact' ? ' active' : ''}}">Contact</a>
                    </li>
                    <!--end::Item-->
                </ul>
                <!--begin::Tab Navs-->
                <!--begin::Tab Content-->
                <div class="tab-content">
                    <!--begin::Tab Pane-->
                    <div class="tab-pane py-5 p-lg-0{{ $routeName == 'home' ? ' active' : ''}}" id="kt_header_tab_home">
                        <!--begin::Menu-->
                        <div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default"></div>
                        <!--end::Menu-->
                    </div>
                    <!--begin::Tab Pane-->

                    <!--begin::Tab Pane-->
                    @php
                        $explode = explode('.', $routeName);
                        $isCertBirth = end($explode) == 'birth';
                        $isCertWeeding = end($explode) == 'weeding';
                        $isCertMissing = end($explode) == 'missing';
                    @endphp
                    <div class="tab-pane p-5 p-lg-0 justify-content-between{{ $isCertPage ? ' active' : '' }}" id="kt_header_tab_certificate">
                        <div class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center">

                            <a href="{{ route('certificate.create.birth') }}" class="btn{{ $isCertBirth ? ' btn-primary' : ' btn-light-primary' }} font-weight-bold mr-3 my-2 my-lg-0">Acte de naissance</a>

                            <a href="{{ route('certificate.create.weeding') }}" class="btn{{ $isCertWeeding ? ' btn-primary' : ' btn-light-primary' }} font-weight-bold mr-3 my-2 my-lg-0">Acte de mariage</a>

                            <a href="{{ route('certificate.create.missing') }}" class="btn{{ $isCertMissing ? ' btn-primary' : ' btn-light-primary' }} font-weight-bold mr-3 my-2 my-lg-0">Acte de décès</a>
                            <!--end::Actions-->
                        </div>
                    </div>

                    <!--begin::Tab Pane-->
                    <div class="tab-pane p-5 p-lg-0 justify-content-between{{ $routeName == 'contact' ? ' active' : ''}}" id="kt_header_tab_contact">
                        <div class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center"></div>
                    </div>
                    <!--begin::Tab Pane-->
                </div>
                <!--end::Tab Content-->
            </div>
            <!--end::Header Menu Wrapper-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Bottom-->
