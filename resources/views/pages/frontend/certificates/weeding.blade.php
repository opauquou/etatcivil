@extends('layouts.master')
@section('title', '| Creation: acte de mariage')

@section('page_styles')
    <link href="/assets/css/pages/wizard/wizard-2.css?v=7.0.5" rel="stylesheet" type="text/css">
    <style>
        .select2-container--default .select2-selection--single{
            background-color: #F3F6F9;
        }
        .select2-container--default .select2-selection--single{
            border: none;
        }
        .select2-container--default.select2-container--open .select2-selection--single, .select2-container--default.select2-container--focus .select2-selection--single{
            border-color: transparent;
        }
        .select2-container--default .select2-selection--single,
        .select2-container--default.select2-container--open .select2-selection--single,
        .select2-container--default.select2-container--focus .select2-selection--single {
            height: calc(1.5em + 1.65rem + 2px);
        }
    </style>
@endsection

@section('content')
<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mb-5">
                <!--begin::Page Title-->
                <h3 class="text-dark font-weight-bold my-1 mr-5">Formulaire d'acte de mariage</h3>
                <!--end::Page Title-->
            </div>
            <!--end::Page Heading-->
        </div>
        <!--end::Info-->
    </div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="card card-custom">
                <div class="card-body p-0">
                    <!--begin: Wizard-->
                    <div class="wizard wizard-2" id="wizard_cert_weeding" data-wizard-state="first" data-wizard-clickable="false">
                        <!--begin: Wizard Nav-->
                        <div class="wizard-nav border-right py-8 px-8 py-lg-20 px-lg-10">
                            <!--begin::Wizard Step 1 Nav-->
                            <div class="wizard-steps">
                                <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                    <div class="wizard-wrapper">
                                        <div class="wizard-icon">
                                            <span class="svg-icon svg-icon-2x">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                                        <path d="M16.5,4.5 C14.8905,4.5 13.00825,6.32463215 12,7.5 C10.99175,6.32463215 9.1095,4.5 7.5,4.5 C4.651,4.5 3,6.72217984 3,9.55040872 C3,12.6834696 6,16 12,19.5 C18,16 21,12.75 21,9.75 C21,6.92177112 19.349,4.5 16.5,4.5 Z" fill="#000000" fill-rule="nonzero"/>
                                                    </g>
                                                </svg><!--end::Svg Icon-->
                                            </span>
                                        </div>
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">Le mariage</h3>
                                            <div class="wizard-desc">Informations de la cérémonie</div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Wizard Step 1 Nav-->
                                <!--begin::Wizard Step 2 Nav-->
                                <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                                    <div class="wizard-wrapper">
                                        <div class="wizard-icon">
                                            <span class="svg-icon svg-icon-2x">
                                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo7\dist/../src/media/svg/icons\Communication\Group.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                                        <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
                                                    </g>
                                                </svg><!--end::Svg Icon-->
                                            </span>
                                        </div>
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">Les mariés</h3>
                                            <div class="wizard-desc">Identité des époux</div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Wizard Step 2 Nav-->
                                <!--begin::Wizard Step 3 Nav-->
                                <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                                    <div class="wizard-wrapper">
                                        <div class="wizard-icon">
                                            <span class="svg-icon svg-icon-2x">
                                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo7\dist/../src/media/svg/icons\General\Clipboard.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"/>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"/>
                                                        <rect fill="#000000" opacity="0.3" x="7" y="10" width="5" height="2" rx="1"/>
                                                        <rect fill="#000000" opacity="0.3" x="7" y="14" width="9" height="2" rx="1"/>
                                                    </g>
                                                </svg><!--end::Svg Icon-->
                                            </span>
                                        </div>
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">Le démandeur</h3>
                                            <div class="wizard-desc">Identité du démandeur</div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Wizard Step 3 Nav-->
                                <!--begin::Wizard Step 4 Nav-->
                                <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                                    <div class="wizard-wrapper">
                                        <div class="wizard-icon">
                                            <span class="svg-icon svg-icon-2x">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/General/Like.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M9,10 L9,19 L10.1525987,19.3841996 C11.3761964,19.7920655 12.6575468,20 13.9473319,20 L17.5405883,20 C18.9706314,20 20.2018758,18.990621 20.4823303,17.5883484 L21.231529,13.8423552 C21.5564648,12.217676 20.5028146,10.6372006 18.8781353,10.3122648 C18.6189212,10.260422 18.353992,10.2430672 18.0902299,10.2606513 L14.5,10.5 L14.8641964,6.49383981 C14.9326895,5.74041495 14.3774427,5.07411874 13.6240179,5.00562558 C13.5827848,5.00187712 13.5414031,5 13.5,5 L13.5,5 C12.5694044,5 11.7070439,5.48826024 11.2282564,6.28623939 L9,10 Z" fill="#000000"></path>
                                                        <rect fill="#000000" opacity="0.3" x="2" y="9" width="5" height="11" rx="1"></rect>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </div>
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">Résumé</h3>
                                            <div class="wizard-desc">Vérifier et envoyer</div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Wizard Step 4 Nav-->
                            </div>
                        </div>
                        <!--end: Wizard Nav-->
                        <!--begin: Wizard Body-->
                        <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                            <!--begin: Wizard Form-->
                            <div class="row">
                                <div class="offset-xxl-2 col-xxl-8">
                                    <form method="POST" action="{{ route('certificate.store.weeding') }}" class="form fv-plugins-bootstrap fv-plugins-framework" id="weeding_cert_form">
                                        @csrf
                                        <input type="hidden" name="typeCert" value="weeding">
                                        <!--begin: Wizard Step 1-->
                                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                            <h4 class="mb-10 font-weight-bold text-dark">Informations sur le titulaire</h4>
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Date de la cérémonie</label>
                                                <input type="text" class="form-control form-control-solid form-control-lg date" id="weeding_date" name="weeding_date"
                                                       placeholder="Date de la cérémonie de mariage" value="{{ old('weeding_date') }}" readonly>
                                                <div class="fv-plugins-message-container"></div>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container has-success">
                                                <label>Pays où s'est déroulé la cérémonie</label>
                                                <select id="weeding_country" name="weeding_country" class="form-control form-control-solid form-control-lg select2">
                                                    <option value="">Sélectionner</option>
                                                    @foreach($countries as $country)
                                                        <option @if($country->code == 'CI')selected @endif value="{{ $country->code }}">{{ $country->libelle }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="fv-plugins-message-container"></div>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container has-success">
                                                <label>Ville où s'est déroulé la cérémonie</label>
                                                <select id="weeding_city" name="weeding_city" class="form-control form-control-solid form-control-lg select2">
                                                    <option value="">Sélectionner</option>
                                                    @foreach($cities as $city)
                                                        <option @if($city->id == 11551)selected @endif value="{{ $city->id }}">{{ strtoupper($city->libelle) }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="fv-plugins-message-container"></div>
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                        <!--end: Wizard Step 1-->
                                        <!--begin: Wizard Step 2-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <h4 class="mb-10 font-weight-bold text-dark">Informations sur les âmes soeurs</h4>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group fv-plugins-icon-container has-success">
                                                        <label>Nom de l'époux</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="h_lname" placeholder="Nom de famille du marié" value="{{ old('h_lname') }}">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Prénom(s) de l'époux</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="h_fname" placeholder="Prénom(s) du marié" value="{{ old('h_fname') }}">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Date de naissance de l'époux</label>
                                                <input type="text" class="form-control form-control-solid form-control-lg date" name="birth_hdate"
                                                       placeholder="Date de naissance du marié" value="{{ old('birth_hdate') }}" readonly>
                                                <div class="fv-plugins-message-container"></div>
                                            </div>
                                            <!--end::Input-->
                                            <div class="row border-top mt-5 pt-10">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group fv-plugins-icon-container has-success">
                                                        <label>Nom de l'épouse</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="w_lname" placeholder="Nom à la naissance de la mariée" value="{{ old('w_lname') }}">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Prénom(s) de l'épouse</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="w_fname" placeholder="Prénom(s) de la mariée" value="{{ old('w_fname') }}">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Date de naissance de l'épouse</label>
                                                <input type="text" class="form-control form-control-solid form-control-lg date" name="birth_wdate"
                                                       placeholder="Date de naissance de la mariée" value="{{ old('birth_wdate') }}" readonly>
                                                <div class="fv-plugins-message-container"></div>
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                        <!--end: Wizard Step 2-->
                                        <!--begin: Wizard Step 3-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <h4 class="mb-10 font-weight-bold text-dark">Information sur le démandeur</h4>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group fv-plugins-icon-container has-success">
                                                        <label>Nom</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="asker_lname" placeholder="Nom de famille" value="{{ old('asker_lname') }}">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Prénom(s)</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="asker_fname" placeholder="Prénom(s)" value="{{ old('asker_fname') }}">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group fv-plugins-icon-container has-success">
                                                        <label>Numéro</label>
                                                        <input type="tel" class="form-control form-control-solid form-control-lg" name="asker_phone" placeholder="Numéro de téléphone" value="{{ old('asker_phone') }}">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="asker_email" placeholder="Adresse mail" value="{{ old('asker_email') }}">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Lien de parenté</label>
                                                <select id="parenthood" name="parenthood" class="form-control form-control-solid form-control-lg select2" style="width: 100%">
                                                    @foreach($parentHoods as $link)
                                                        <option value="{{ $link->name }}">{{ $link->libelle }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="fv-plugins-message-container"></div>
                                            </div>
                                        </div>
                                        <!--end: Wizard Step 3-->
                                        <!--begin: Wizard Step 4-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <!--begin::Section-->
                                            <h4 class="mb-10 font-weight-bold text-dark">Vérifier vos informations avant de soumettre</h4>
                                            <h6 class="font-weight-bolder mb-3">Cérémonie:</h6>
                                            <div class="text-dark-50 line-height-lg font-size-h5">
                                                <div>Date: <span class="resume weeding_date font-size-h3 font-weight-bolder"></span></div>
                                                <div>Lieu:
                                                    <span class="resume weeding_city font-size-h3 font-weight-bolder"></span>,
                                                    <span class="resume weeding_country font-size-h3 font-weight-bolder"></span>
                                                </div>
                                            </div>
                                            <div class="separator separator-dashed my-5"></div>
                                            <h6 class="font-weight-bolder mb-3">Les époux:</h6>
                                            <div class="text-dark-50 line-height-lg font-size-h5">
                                                <div>Nom de l'époux: <span class="resume h_lname font-size-h3 font-weight-bolder"></span></div>
                                                <div>Prénom(s) de l'époux: <span class="resume h_fname font-size-h3 font-weight-bolder"></span></div>
                                                <div>Date: <span class="resume birth_hdate font-size-h3 font-weight-bolder"></span></div>
                                                <br/>
                                                <div>Nom de l'époux: <span class="resume w_lname font-size-h3 font-weight-bolder"></span></div>
                                                <div>Prénom(s) de l'époux: <span class="resume w_fname font-size-h3 font-weight-bolder"></span></div>
                                                <div>Date: <span class="resume birth_wdate font-size-h3 font-weight-bolder"></span></div>
                                            </div>
                                            <div class="separator separator-dashed my-5"></div>
                                            <h6 class="font-weight-bolder mb-3">Démandeur:</h6>
                                            <div class="text-dark-50 line-height-lg font-size-h5">
                                                <div>Nom: <span class="resume asker_lname font-size-h3 font-weight-bolder"></span></div>
                                                <div>Prénom(s): <span class="resume asker_fname font-size-h3 font-weight-bolder"></span></div>
                                                <div>Téléphone: <span class="resume asker_phone font-size-h3 font-weight-bolder"></span></div>
                                                <div>Email: <span class="resume asker_email font-size-h3 font-weight-bolder"></span></div>
                                                <div>Lien de parenté: <span class="resume parenthood font-size-h3 font-weight-bolder"></span></div>
                                            </div>
                                            <!--end::Section-->
                                        </div>
                                        <!--end: Wizard Step 4-->
                                        <!--begin: Wizard Actions-->
                                        <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                            <div class="mr-2">
                                                <button type="button" class="btn btn-light-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-prev">Précédent</button>
                                            </div>
                                            <div>
                                                <button type="submit" class="btn btn-success font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-submit">Envoyer</button>
                                                <button type="button" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-next">Suivant</button>
                                            </div>
                                        </div>
                                        <!--end: Wizard Actions-->
                                    </form>
                                </div>
                                <!--end: Wizard-->
                            </div>
                            <!--end: Wizard Form-->
                        </div>
                        <!--end: Wizard Body-->
                    </div>
                    <!--end: Wizard-->
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
@endsection

@section('page_scripts')
    <script src="{{ asset('assets/js/pages/custom/wizard/wizard-cert-weeding.js') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/select2.js') }}"></script>

    <script>
        // basic
        $('.select2').select2({
            placeholder: "Selectionner"
        });

        $('input[type=tel]').inputmask({
            "mask": "9",
            "repeat": 15,
            "greedy": false
        });

        $('.date').datepicker({
            rtl: KTUtil.isRTL(),
            orientation: "top left",
            format: "dd-mm-yyyy",
            todayHighlight: true,
            todayBtn: "linked",
            clearBtn: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

        $('#weeding_country').on('change', function (e) {
            const wCountry = $(this).val();

            $.get('/ajax/country-cities?country_code='+ wCountry, function(data){
                $('select[name=weeding_city]').empty().append('<option value="" data-hidden="true">Selectionner</option>');
                if(data.length){
                    $.each(data, function(index, city){
                        $('select[name=weeding_city]')
                        .append('<option value="' + city.code + '">' + city.libelle.toUpperCase() + '</option>');
                    });
                }
                $('select[name=weeding_city]').selectpicker('refresh');
            });
        });
    </script>
@endsection
