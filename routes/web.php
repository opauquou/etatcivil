<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'PageController@index']);
Route::get('/contact', ['as' => 'contact', 'uses' => 'PageController@contact']);

Route::group(['prefix' => 'certificate'], function(){
   Route::group(['prefix' => 'create'], function(){
       Route::get('/birth', ['as' => 'certificate.create.birth', 'uses' => 'BirthCertificateController@create']);
       Route::post('/birth', ['as' => 'certificate.store.birth', 'uses' => 'BirthCertificateController@store']);
    
       Route::get('/weeding', ['as' =>'certificate.create.weeding', 'uses' => 'WeedingCertificateController@create']);
       Route::post('/weeding', ['as' =>'certificate.store.weeding', 'uses' => 'WeedingCertificateController@store']);
    
       Route::get('/missing', ['as' =>'certificate.create.missing', 'uses' => 'MissingCertificateController@create']);
       Route::post('/missing', ['as' =>'certificate.store.missing', 'uses' => 'MissingCertificateController@store']);
   });
});

Route::group(['prefix' => 'ajax'], function (){
    Route::get('/country-cities', ['as' =>'ajax.country-cities', 'uses' => 'AjaxController@citiesOfCountry']);
    Route::get('/get-parenthood', ['as' =>'ajax.get-parenthood', 'uses' => 'AjaxController@getParenthood']);
    Route::get('/get-country', ['as' =>'ajax.get-country', 'uses' => 'AjaxController@getCountry']);
    Route::get('/get-city', ['as' =>'ajax.get-city', 'uses' => 'AjaxController@getCity']);
});


/**
 * login routes
 */
Auth::routes();


#### ADMISTRATION ###
Route::group(['prefix' => 'admin'], function (){
   Route::get('/', 'AdminController@index')->name('dashboard');
   
   Route::group(['prefix' => 'certificate'], function (){
       Route::get('birth', 'BirthCertificateController@index')->name('certificate.list.birth');
       Route::get('weeding', 'WeedingCertificateController@index')->name('certificate.list.weeding');
       Route::get('missing', 'MissingCertificateController@index')->name('certificate.list.missing');
   });
   
   Route::group(['prefix' => 'ajax'], function (){
       Route::group(['prefix' => 'datatable'], function (){
           Route::get('/birth', 'BirthCertificateController@getData');
           Route::get('/weeding', 'WeedingCertificateController@getData');
           Route::get('/missing', 'MissingCertificateController@getData');
       });
   });
});
