$('#kt_datatable_weeding').DataTable({
    "oLanguage": {
        "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
        "sInfo": "Page _PAGE_ sur _PAGES_",
        "sSearch": '',
        "sSearchPlaceholder": "Rechercher...",
        "sLengthMenu": "Resultats :  _MENU_",
        "sProcessing":     "Chargement des données..."
    },
    "stripeClasses": [],
    "lengthMenu": [15, 30, 50],
    "pageLength": 15,
    processing: true,
    serverSide: true,
    ajax: '/admin/ajax/datatable/weeding',
    columns: [
        {data: 'id', name: 'id', orderable: false, searchable: false},
        {data: 'wc_weeding_date', name: 'wc_weeding_date', orderable: false, searchable: true},
        {data: 'husband', name: 'husband', orderable: false, searchable: true},
        {data: 'wife', name: 'wife', orderable: false, searchable: true},
        {data: 'created_at', name: 'created_at', orderable: false, searchable: true},
        {data: 'actions', name: 'actions', orderable: false, searchable: false}
    ]
});
