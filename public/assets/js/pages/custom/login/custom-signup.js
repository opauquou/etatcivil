"use strict";

// Class Definition
var KTLogin = function() {
    var signUpForm;
    
    var _handleFormSignup = function() {
        // Base elements
        var wizardEl = KTUtil.getById('kt_login');
        var form = KTUtil.getById('kt_login_signup_form');
        var wizardObj;
        var validations = [];
        
        if (!form) {
            return;
        }
        
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        // Step 1
        validations.push(FormValidation.formValidation(
            form,
            {
                fields: {
                    first_name: {
                        validators: {
                            notEmpty: {
                                message: "Le/les prénom(s) est/sont obligatoire(s)"
                            }
                        }
                    },
                    last_name: {
                        validators: {
                            notEmpty: {
                                message: 'Le nom de famille est obligatoire'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
        
        // Step 2
        validations.push(FormValidation.formValidation(
            form,
            {
                fields: {
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'L\'email est obligatoire'
                            },
                            emailAddress: {
                                message: 'Veuillez entrer une adresse email valide'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Le mot de passe est obligatoire'
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'La confirmation du mot de passe est obligatoire'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
        
        
        // Initialize form wizard
        wizardObj = new KTWizard(wizardEl, {
            startStep: 1, // initial active step number
            clickableSteps: false // to make steps clickable this set value true and add data-wizard-clickable="true" in HTML for class="wizard" element
        });
        
        // Validation before going to next page
        wizardObj.on('beforeNext', function (wizard) {
            validations[wizard.getStep() - 1].validate().then(function (status) {
                if (status == 'Valid') {
                    summary(signUpForm);
                    wizardObj.goNext();
                    KTUtil.scrollTop();
                } else {
                    Swal.fire({
                        text: "Sorry, looks like there are some errors detected, please try again.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }
            });
            
            wizardObj.stop();  // Don't go to the next step
        });
        
        // Change event
        wizardObj.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    }
    
    var summary = function(elForm){
        var formData = elForm.serialize().split('&');
        $.each(formData, function(i, value){
            value = value.split("=");
            if(value[0] === 'email'){
                $('.resume.'+value[0]).html(value[1].replace(/%40/g, '@'));
            }else{
                $('.resume.'+value[0]).html(decodeURI(value[1]).toUpperCase());
            }
        })
    }
    
    // Public Functions
    return {
        init: function() {
            signUpForm = $('#kt_login_signup_form');
            
            _handleFormSignup();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTLogin.init();
});
