"use strict";

// Class definition
var KTWizardBirth = function () {
    // Base elements
    var _wizardEl;
    var _formEl;
    var _wizard;
    var _validations = [];
    var form;
    var mCountry, mbCountry;
    var mCity, mbCity;
    var hood;
    
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        _wizard = new KTWizard(_wizardEl, {
            startStep: 1, // initial active step number
            clickableSteps: false // to make steps clickable this set value true and add data-wizard-clickable="true" in HTML for class="wizard" element
        });
        
        // Validation before going to next page
        _wizard.on('beforeNext', function (wizard) {
            // Don't go to the next step yet
            _wizard.stop();
            
            // Validate form
            var validator = _validations[wizard.getStep() - 1]; // get validator for current step
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    summary(form);
                    _wizard.goNext();
                    KTUtil.scrollTop();
                } else {
                    Swal.fire({
                        text: "Oops, des erreurs ont été trouvées. Veuillez les corriger et rééssayer.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "OK",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }
            });
        });
        
        // Change event
        _wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    }
    
    var initValidation = function () {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        // Step 1
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    missing_date: {
                        validators: {
                            notEmpty: {
                                message: 'La date de décès est obligatoire.'
                            }
                        }
                    },
                    missing_country: {
                        validators: {
                            notEmpty: {
                                message: 'Veuillez sélectionnner le pays où est survenu le décès.'
                            }
                        }
                    },
                    missing_city: {
                        validators: {
                            notEmpty: {
                                message: 'Veuillez sélectionnner la ville où est survenu le décès.'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
        
        // Step 2
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    missing_lname: {
                        validators: {
                            notEmpty: {
                                message: 'Le nom du défunt est obligatoire.'
                            }
                        }
                    },
                    missing_fname: {
                        validators: {
                            notEmpty: {
                                message: 'Le/les prénom(s) du défunt est/sont obligatoire(s).'
                            }
                        }
                    },
                    missing_birth_date: {
                        validators: {
                            notEmpty: {
                                message: 'La date de naissance du défunt est obligagtoire'
                            }
                        }
                    },
                    missing_birth_country: {
                        validators: {
                            notEmpty: {
                                message: 'Veuillez sélectionnner le pays de naissance du défunt.'
                            }
                        }
                    },
                    missing_birth_city: {
                        validators: {
                            notEmpty: {
                                message: 'Veuillez sélectionnner la ville de naissance du défunt.'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
        
        // Step 3
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    asker_lname: {
                        validators: {
                            notEmpty: {
                                message: 'Votre nom est obligatoire'
                            }
                        }
                    },
                    asker_fname: {
                        validators: {
                            notEmpty: {
                                message: 'Votre/vos prénom(s) est/sont obligatoite(s)'
                            }
                        }
                    },
                    asker_phone: {
                        validators: {
                            notEmpty: {
                                message: 'Votre numéro est obligatoire'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
    }
    
    var summary = function(form){
        var formData = form.serialize().split('&');
        $.each(formData, function(i, value){
            value = value.split("=");
            if(value[0] === 'missing_country'){
                if(value[1] !== mCountry){
                    $.get('/ajax/get-country?country_code='+ value[1], function(data){
                        $('.resume.'+value[0]).html(data.libelle);
                    });
                    mCountry = value[1];
                }
            }else if(value[0] === 'missing_city'){
                if(mCity !== value[1]) {
                    $.get('/ajax/get-city?city_id=' + value[1], function (data) {
                        $('.resume.' + value[0]).html(data.libelle[0] + data.libelle.substring(1).toLowerCase());
                    });
                    mCity = value[1];
                }
            }else if(value[0] === 'missing_birth_country'){
                if(value[1] !== mbCountry){
                    $.get('/ajax/get-country?country_code='+ value[1], function(data){
                        $('.resume.'+value[0]).html(data.libelle);
                    });
                    mCountry = value[1];
                }
            }else if(value[0] === 'missing_birth_city'){
                if(value[1] != mbCity) {
                    $.get('/ajax/get-city?city_id=' + value[1], function (data) {
                        $('.resume.' + value[0]).html(data.libelle[0] + data.libelle.substring(1).toLowerCase());
                    });
                    mbCity = value[1];
                }
            }else if(value[0].slice(-4) === 'date'){
                $('.resume.'+value[0]).html(value[1].replace(/-/g, '/'));
            }else if(value[0] == 'parenthood'){
                if(value[1] != hood) {
                    $.get('/ajax/get-parenthood?name=' + value[1], function (data) {
                        $('.resume.' + value[0]).html(data.libelle[0] + data.libelle.substring(1).toLowerCase());
                    });
                    hood = value[1];
                }
            }else if(value[0] === 'asker_email'){
                $('.resume.'+value[0]).html(value[1].replace(/%40/g, '@'));
            }else{
                $('.resume.'+value[0]).html(decodeURI(value[1]).toUpperCase());
            }
        })
    }
    
    return {
        // public functions
        init: function () {
            _wizardEl = KTUtil.getById('wizard_cert_missing');
            _formEl = KTUtil.getById('missing_cert_form');
            form = $('#missing_cert_form');
            
            initWizard();
            initValidation();
        }
    };
}();

jQuery(document).ready(function () {
    KTWizardBirth.init();
});
