"use strict";

// Class definition
var KTWizardBirth = function () {
    // Base elements
    var _wizardEl;
    var _formEl;
    var _wizard;
    var _validations = [];
    var form;
    var bCountry;
    var bCity;
    var pHood;
    
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        _wizard = new KTWizard(_wizardEl, {
            startStep: 1, // initial active step number
            clickableSteps: false // to make steps clickable this set value true and add data-wizard-clickable="true" in HTML for class="wizard" element
        });
        
        // Validation before going to next page
        _wizard.on('beforeNext', function (wizard) {
            // Don't go to the next step yet
            _wizard.stop();
            
            // Validate form
            var validator = _validations[wizard.getStep() - 1]; // get validator for current step
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    summary(form);
                    _wizard.goNext();
                    KTUtil.scrollTop();
                } else {
                    Swal.fire({
                        text: "Oops, des erreurs ont été trouvées. Veuillez les corriger et rééssayer.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "OK",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }
            });
        });
        
        // Change event
        _wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    }
    
    var initValidation = function () {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        // Step 1
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    fname: {
                        validators: {
                            notEmpty: {
                                message: "Le/les prénom(s) du titulaire est/sont obligatoire(s)."
                            }
                        }
                    },
                    lname: {
                        validators: {
                            notEmpty: {
                                message: 'Le nom de famille du titulaire est obligatoire.'
                            }
                        }
                    },
                    birth_date: {
                        validators: {
                            notEmpty: {
                                message: 'La date de naissance du titulaire est obligatoire.'
                            }
                        }
                    },
                    birth_country: {
                        validators: {
                            notEmpty: {
                                message: 'Veuillez sélectionner le pays de naissance.'
                            }
                        }
                    },
                    birth_city: {
                        validators: {
                            notEmpty: {
                                message: 'Veuillez sélectionner la ville de naissance.'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
        
        // Step 2
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    father_lname: {
                        validators: {
                            notEmpty: {
                                message: 'Le nom du père est obligatoire'
                            }
                        }
                    },
                    father_fname: {
                        validators: {
                            notEmpty: {
                                message: "Le/les prénom(s) du père est/sont obligatoire(s)"
                            }
                        }
                    },
                    mother_lname: {
                        validators: {
                            notEmpty: {
                                message: 'Le nom de la mère est obligatoire'
                            }
                        }
                    },
                    mother_fname: {
                        validators: {
                            notEmpty: {
                                message: "Le/les prénom(s) de la mère est/sont obligatoire(s)"
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
        
        // Step 3
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    asker_lname: {
                        validators: {
                            notEmpty: {
                                message: 'Votre nom est obligatoire'
                            }
                        }
                    },
                    asker_fname: {
                        validators: {
                            notEmpty: {
                                message: "Votre/vos prénom(s) est/sont obligatoite(s)"
                            }
                        }
                    },
                    asker_phone: {
                        validators: {
                            notEmpty: {
                                message: 'Votre numéro est obligatoire'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
    }
    
    var summary = function(form){
        var formData = form.serialize().split('&');
        $.each(formData, function(i, value){
            value = value.split("=");
            if(value[0] === 'birth_country'){
                if(value[1] !== bCountry){
                    $.get('/ajax/get-country?country_code='+ value[1], function(data){
                        $('.resume.'+value[0]).html(data.libelle);
                    });
                    bCountry = value[1];
                }
            }else if(value[0] === 'birth_city'){
                if(value[1] !== bCity) {
                    $.get('/ajax/get-city?city_id=' + value[1], function (data) {
                        $('.resume.' + value[0]).html(data.libelle[0] + data.libelle.substring(1).toLowerCase());
                    });
                    bCity = value[1];
                }
            }else if(value[0].slice(-4) === 'date'){
                $('.resume.'+value[0]).html(value[1].replace(/-/g, '/'));
            }else if(value[0] === 'asker_email'){
                $('.resume.'+value[0]).html(value[1].replace(/%40/g, '@'));
            }else if(value[0] === 'parenthood'){
                if(value[1] !== pHood) {
                    $.get('/ajax/get-parenthood?name=' + value[1], function (data) {
                        $('.resume.' + value[0]).html(data.libelle[0] + data.libelle.substring(1).toLowerCase());
                    });
                    pHood = value[1];
                }
            }else{
                $('.resume.'+value[0]).html(decodeURI(value[1]).toUpperCase());
            }
        })
    }
    
    return {
        // public functions
        init: function () {
            _wizardEl = KTUtil.getById('wizard_cert_birth');
            _formEl = KTUtil.getById('birth_cert_form');
            form = $('#birth_cert_form');
            
            initWizard();
            initValidation();
        }
    };
}();

jQuery(document).ready(function () {
    KTWizardBirth.init();
});
