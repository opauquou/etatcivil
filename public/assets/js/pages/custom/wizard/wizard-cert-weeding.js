"use strict";

// Class definition
var KTWizardBirth = function () {
    // Base elements
    var _wizardEl;
    var _formEl;
    var _wizard;
    var _validations = [];
    var form;
    var wContry;
    var wCity;
    var pHood;
    
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        _wizard = new KTWizard(_wizardEl, {
            startStep: 1, // initial active step number
            clickableSteps: false // to make steps clickable this set value true and add data-wizard-clickable="true" in HTML for class="wizard" element
        });
        
        // Validation before going to next page
        _wizard.on('beforeNext', function (wizard) {
            // Don't go to the next step yet
            _wizard.stop();
            
            // Validate form
            var validator = _validations[wizard.getStep() - 1]; // get validator for current step
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    summary(form);
                    _wizard.goNext();
                    KTUtil.scrollTop();
                } else {
                    Swal.fire({
                        text: "Oops, des erreurs ont été trouvées. Veuillez les corriger et rééssayer.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "OK",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }
            });
        });
        
        // Change event
        _wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    }
    
    var initValidation = function () {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        // Step 1
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    weeding_date: {
                        validators: {
                            notEmpty: {
                                message: 'La date du mariage est obligatoire'
                            }
                        }
                    },
                    weeding_country: {
                        validators: {
                            notEmpty: {
                                message: 'Veuillez sélectionner le pays où a eu lieu la cérémonie de mariage'
                            }
                        }
                    },
                    weeding_city: {
                        validators: {
                            notEmpty: {
                                message: 'Veuillez sélectionner la ville où a eu lieu la cérémonie de mariage'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
        
        // Step 2
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    h_lname: {
                        validators: {
                            notEmpty: {
                                message: "Le nom de l'époux est obligatoire"
                            }
                        }
                    },
                    h_fname: {
                        validators: {
                            notEmpty: {
                                message: "Le/les prénom(s) de l'époux est/sont obligatoire(s)"
                            }
                        }
                    },
                    birth_hdate: {
                        validators: {
                            notEmpty: {
                                message: "La date de naissance de l'époux est obligatoire"
                            }
                        }
                    },
                    w_lname: {
                        validators: {
                            notEmpty: {
                                message: "Le nom de l'épouse est obligatoire"
                            }
                        }
                    },
                    w_fname: {
                        validators: {
                            notEmpty: {
                                message: "Le/les prénom(s) de l'épouse est/sont obligatoire(s)"
                            }
                        }
                    },
                    birth_wdate: {
                        validators: {
                            notEmpty: {
                                message: "La date de naissance de l'épouse est obligatoire"
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
        
        // Step 3
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    asker_lname: {
                        validators: {
                            notEmpty: {
                                message: 'Votre nom est obligatoire'
                            }
                        }
                    },
                    asker_fname: {
                        validators: {
                            notEmpty: {
                                message: 'Votre/vos prénom(s) est/sont obligatoite(s)'
                            }
                        }
                    },
                    asker_phone: {
                        validators: {
                            notEmpty: {
                                message: 'Votre numéro est obligatoire'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
    }
    
    var summary = function(form){
        var formData = form.serialize().split('&');
        $.each(formData, function(i, value){
            value = value.split("=");
            if(value[0] === 'weeding_country'){
                if(value[1] !== wContry){
                    $.get('/ajax/get-country?country_code='+ value[1], function(data){
                        $('.resume.'+value[0]).html(data.libelle);
                    });
                    wContry = value[1];
                }
            }else if(value[0] === 'weeding_city'){
                if(wCity !== value[1]) {
                    $.get('/ajax/get-city?city_id=' + value[1], function (data) {
                        $('.resume.' + value[0]).html(data.libelle[0] + data.libelle.substring(1).toLowerCase());
                    });
                    wCity = value[1];
                }
            }else if(value[0] === 'parenthood'){
                if(pHood !== value[1]) {
                    $.get('/ajax/get-parenthood?name=' + value[1], function (data) {
                        $('.resume.' + value[0]).html(data.libelle[0] + data.libelle.substring(1).toLowerCase());
                    });
                    pHood = value[1];
                }
            }else if(value[0].slice(-4) === 'date'){
                $('.resume.'+value[0]).html(value[1].replace(/-/g, '/'));
            }else if(value[0] === 'asker_email'){
                $('.resume.'+value[0]).html(value[1].replace(/%40/g, '@'));
            }else{
                $('.resume.'+value[0]).html(decodeURI(value[1]).toUpperCase());
            }
        })
    }
    
    return {
        // public functions
        init: function () {
            _wizardEl = KTUtil.getById('wizard_cert_weeding');
            _formEl = KTUtil.getById('weeding_cert_form');
            form = $('#weeding_cert_form');
            
            initWizard();
            initValidation();
        }
    };
}();

jQuery(document).ready(function () {
    KTWizardBirth.init();
});
